package dethi;

public class Giangvien extends connguoi {
	private String magv;
	private Double phucap;
	public Giangvien() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Giangvien(String hoten, String loaiHD, Double hsl) {
		super(hoten, loaiHD, hsl);
		// TODO Auto-generated constructor stub
	}
	public Giangvien(String magv,String hoten, String loaiHD, Double hsl, Double phucap) {
		super(hoten, loaiHD, hsl);
		this.magv = magv;
		this.phucap = phucap;
	}
	public String getMagv() {
		return magv;
	}
	public void setMagv(String magv) {
		this.magv = magv;
	}
	public Double getPhucap() {
		return phucap;
	}
	public void setPhucap(Double phucap) {
		this.phucap = phucap;
	}
	@Override
	public String toString() {
		return "Giangvien [magv=" + magv + ", phucap=" + phucap + "]";
	}
}
