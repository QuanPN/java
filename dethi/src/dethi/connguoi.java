package dethi;

public class connguoi {
	private String hoten;
	private String loaiHD;
	private Double hsl;
	
	public connguoi() {
		super();
		// TODO Auto-generated constructor stub
	}
	public connguoi(String hoten, String loaiHD, Double hsl) {
		super();
		this.hoten = hoten;
		this.loaiHD = loaiHD;
		this.hsl = hsl;
	}
	public String getHoten() {
		return hoten;
	}
	public void setHoten(String hoten) {
		this.hoten = hoten;
	}
	public String getLoaiHD() {
		return loaiHD;
	}
	public void setLoaiHD(String loaiHD) {
		this.loaiHD = loaiHD;
	}
	public Double getHsl() {
		return hsl;
	}
	public void setHsl(Double hsl) {
		this.hsl = hsl;
	}
	@Override
	public String toString() {
		return "connguoi [hoten=" + hoten + ", loaiHD=" + loaiHD + ", hsl=" + hsl + "]";
	}
	
	
}
