package dethi;

public class Nhanvien extends connguoi {
	private String manv;

	public Nhanvien(String manv,String hoten, String loaiHD, Double hsl) {
		super(hoten, loaiHD, hsl);
		this.manv = manv;
	}

	public Nhanvien() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Nhanvien(String hoten, String loaiHD, Double hsl) {
		super(hoten, loaiHD, hsl);
		// TODO Auto-generated constructor stub
	}

	public String getManv() {
		return manv;
	}

	public void setManv(String manv) {
		this.manv = manv;
	}

	@Override
	public String toString() {
		return manv+" "+getHoten()+" "+getLoaiHD()+" "+getHsl();
	}
	
	
}
