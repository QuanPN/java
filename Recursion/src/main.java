import java.lang.reflect.Array;
import java.util.Arrays;

public class main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Recursion recursion = new Recursion();
		
		System.out.println("Calculate the Sum of two numbers: "
		                    + recursion.sumOfTwoNumbers(6, 8));
		
		System.out.println("Calculate the Product of two numbers: " 
		                    + recursion.productOfTwoNumbers(6, 8));
		
		System.out.println("Calculate the Power of two numbers: "
		                    + recursion.powerOfTwoNumbers(6, 8));
		
		System.out.println("Finding the GCD (Greatest Common Divisor) of two numbers: "
		                    + recursion.gcd(6, 8));

		int[] arr1 = {214, 12, 4253, 5, 23, 3, 21, 314, 5, 5, 5, 7, 6, 4, 8};		
		recursion.reverseArray(arr1, 0, arr1.length - 1);
		System.out.println("Reverse an array: " + Arrays.toString(arr1));
		
		int[] arr2 = {2, 6, 7, 9, 11, 14, 55, 66};
		System.out.println("Apply the  recursion Algorithms for Binary search Algorithm: "
		                    + recursion.recursion_BinarySearch(arr2, 2, 0, arr2.length - 1));
		
		int[] arr3 = {234, 23, 4, 124, 1341, 24, 124, 1, 24, 12, 4};
 		recursion.mergeSort(arr3, 0, arr3.length - 1);
		System.out.println("Apply the  recursion Algorithms for Merge Sort Algorithm: "
		                    + Arrays.toString(arr3));
		
		int[] arr4 = {324, 2, 34, 235, 1234, 124, 1, 34, 35, 53, 5};
		recursion.quickSort(arr4, 0, arr4.length - 1);
		System.out.println("Apply the  recursion Algorithms for Quick Sort algorithm: "
		                    + Arrays.toString(arr4));
		
		int[] arr5 = {2, 3, 23, 423, 42, 34, 234, 235, 5, 6, 66, 6, 6, 32423, 4};
		System.out.println("Max to arr3: " + recursion.max(arr4, 0, arr4[0]));
		System.out.println("Min to arr3: " + recursion.min(arr4, 0, arr4[0]));
		
		String s1 = "azxxzy";
		String s2 = "geeksforgeeg";
		String s3 = "caaabbbaacdddd";
		String s4 = "acaaabbbacdddd";
		
		System.out.println("S1: " + recursion.problem_4(s1));
		System.out.println("S2: " + recursion.problem_4(s2));
		System.out.println("S3: " + recursion.problem_4(s3));
		System.out.println("S4: " + recursion.problem_4(s4));	
	}

}
