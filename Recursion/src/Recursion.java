/**
 * 
 * @author QuanPN
 *
 */
public class Recursion {
    
	/**
	 * 
	 * @param a
	 * @param b
	 * @return sum of two numbers
	 */
	public static int sumOfTwoNumbers(int a, int b) {
		if(b == 0) {
			return a;
		}
		
		return sumOfTwoNumbers((a ^ b), (a & b) << 1);
	}
	
	/**
	 * 
	 * @param a
	 * @param b
	 * @return Greatest Common Divisor of two numbers
	 */
	public static int gcd(int a, int b) {
	        if (b == 0) {
	        	return a;
	        }else {
	        	return gcd(b, a % b);
	        }        
	}
	
	/**
	 * 
	 * @param a
	 * @param b
	 * @return product of two numbers
	 */
	public static int productOfTwoNumbers(int a, int b) {
		if (b == 0) {
			return 0;
		}else if (b > 0) {
			return a + productOfTwoNumbers(a, b - 1);
		}else{
			return -productOfTwoNumbers(a, -b);
		}
	}
	
	/**
	 * 
	 * @param a
	 * @param b
	 * @return power of two numbers
	 */
	public static int powerOfTwoNumbers(int a, int b) {
		if (b == 0) {
			return 1;
		}else if (b > 0) {
			return a * powerOfTwoNumbers(a, b - 1);
		}else {
			 return 1 / (a * powerOfTwoNumbers(a, -b - 1));
		}
	}
	
	 /**
     * 
     * @param a
     * @param b
     */
    static void swap(int[] array, int i, int j) {
        int temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }	
    
	/**
	 * 
	 * @param array
	 * @param start
	 * @param end
	 */
	public static void reverseArray(int[] array, int start, int end) {
		if(start >= end) {
			return;
		}
		
		swap(array, start, end);
			
		reverseArray(array, start + 1, end - 1);
	}
	
	/**
	 * 
	 * @param array
	 * @param key
	 * @param left
	 * @param right
	 * @return largest number position
	 */
	public static int recursion_BinarySearch(int[] array, int key, int left, int right) {
	    if (left <= right) {
	        int mid = (left + right) / 2;
	        
	        if (array[mid] == key) {
	            return mid;
	        } else if (array[mid] < key) {
	            return recursion_BinarySearch(array, key, mid + 1, right);
	        } else {
	            return recursion_BinarySearch(array, key, left, mid - 1);
	        }
	    }
	    
	    return -1; 
	}
	
	/**
	 * 
	 * @param array
	 * @param low
	 * @param high
	 */
    public void quickSort(int[] array, int low, int high){
        int p = array[(low + high) / 2];
        int i = low;
        int j = high;
        
        while (true){
            
            while (array[i] < p){
                i++;
            }
            
            while (array[j] > p){
                j--;
            }
            
            if(i > j) {
                break;
            }else {
                swap(array, i, j);
                i++;
                j--;
            }
            
        }
        
        if (i < high){
            quickSort(array, i, high);
        }
        
        if (low < j){
            quickSort(array, low, j);
        }
    }
    
	/**
	 * 
	 * @param arr
	 * @param l
	 * @param m
	 * @param r
	 */
     static void merge(int array[], int left, int mid, int right) {       
        int n1 = mid - left + 1;
        int n2 = right - mid;
        int L[] = new int[n1];
        int R[] = new int[n2];

        for (int i = 0; i < n1; ++i) {
            L[i] = array[left + i];
        }
        
        for (int j = 0; j < n2; ++j) {
            R[j] = array[mid + 1 + j];
        }

        int i = 0;
        int j = 0;
        int k = left;
        
        while (i < n1 && j < n2) {
            if (L[i] <= R[j]) {
                array[k] = L[i];
                i++;
            }else {
                array[k] = R[j];
                j++;
            }
            
            k++;
        }

        while (i < n1) {
            array[k] = L[i];
            i++;
            k++;
        }
        
        while (j < n2) {
            array[k] = R[j];
            j++;
            k++;
        }
    }
    /**
     * 
     * @param arr
     * @param l
     * @param r
     */
	public static void mergeSort(int[] arr, int l, int r) {
	    if (l < r) {
            int m = l + (r - l) / 2;

            mergeSort(arr, l, m);
            mergeSort(arr, m + 1, r);
            merge(arr, l, m, r);
        }
	}
	
	/**
	 * 
	 * @param array
	 * @param i
	 * @param max
	 * @return largest number position
	 */
	public static int max(int[] array, int i, int max) {
	    if(i == array.length) {
	        return max;
	    }
	    
	    if(max < array[i]) {
	        max = array[i];
	    }
	    
	    return max(array, i + 1, max);
	}
	
	/**
	 * 
	 * @param array
	 * @param i
	 * @param min
	 * @return smallest number position
	 */
	public static int min(int[] array, int i, int min) {
	    if(i == array.length) {
            return min;
        }
        
        if(min > array[i]) {
            min = array[i];
        }
        
	    return min(array, i + 1, min);
	}
	
	//Problem 4: Given a string, use recursion algorithms 
	//to remove adjacent duplicate characters from the string.
	//The output string should not have any adjacent duplicates
	/**
	 * 
	 * @param str
	 * @return true or false
	 */
	public static boolean checkString(String str) {
	    for(int i = 0; i < str.length() - 1; i++) {
	      //Nếu có 2 ký tự trùng lặp thì trả về true
	        if(str.charAt(i) == str.charAt(i + 1)) { 
	            return true;
	        }
	    }
	        
	    return false;
	}
	/**
	 * 
	 * @param st
	 * @return string should not have any adjacent duplicates
	 */
	public static String problem_4(String st) {
	    
	    //Nếu chuỗi vào là rỗng thì trả về 'Empty string'
	    if(st.equals("")) {
	        return "Empty String";
	    }
	     
	    //Nếu chuỗi vào không có ký tự nào trùng lặp nằm gần nhau trả về chuỗi đó
	    if(!checkString(st)) {
	        return st;
	    }
	    
	    String temp = "";
        int cnt = 1;
        
        for(int i = 0; i <= st.length() - 1; i++) {
            
            //Nếu đây là ký tự cuối cùng
            if((i == st.length() - 1)) {
                //Nếu ký tự này không bị lặp thì nối vào temp
                if(cnt == 1) {
                    temp += st.charAt(i);
                }
                //Thoát vòng lặp
                break;               
            }
            
            //Nếu có 2 ký tự liền nhau và giống nhau
            if(st.charAt(i) == st.charAt(i + 1)) {
                //Tăng biến đếm
                cnt++;
            }else { //Ngược lại
                if(cnt == 1) { //Nếu biến cnt = 1 (tức là ký tự này không bị trùng lặp) thì nối vào temp
                    temp += st.charAt(i);
                }
                //Gán lại biến đếm bằng 1
                cnt = 1;                
            }
        }
        
	    return problem_4(temp);
	}
}