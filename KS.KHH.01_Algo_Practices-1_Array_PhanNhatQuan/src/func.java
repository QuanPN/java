import java.util.ArrayList;
import java.util.Arrays;

public class func {
	
	public int[] Problem1_1(int[] a) {
		
		int[] lst = new int[a.length];
		
		int count = 0;
		
		for(int i = 0; i < a.length ; i++) {
			if(a[i] != 0) {
				lst[count] = a[i];
				count++;
			}
		}
		
		return lst;
	}
	
	public int[] Problem1_2(int[] a) {
		
		int l = 0, r = a.length - 1;
		
		while(r > l) {
			if(a[l] == 0 && a[r] != 0) {
				int tmp = a[r];
				a[r] = a[l];
				a[l] = tmp;
				l++;
				r--;
			}else if(a[l] != 0 && a[r] == 0) {
				r--;
			}else {
				l++;
			}
		}
		
		return a;
	}
	
	
	public int[] Problem2_1(int[] a) {
		
		int count_0 = 0, count_1 = 0;
		
		for(int i: a) {
			if(i == 0) count_0++;
			if(i == 1) count_1++;
		}
		
		for(int i = 0; i < a.length; i++) {
			if(i < count_0) {
				a[i] = 0;
			}else if(i < count_0 + count_1) {
				 a[i] = 1;
			}else {
				 a[i] = 2;
			}
		}
		
		return a;
	}
	
	public int[] Problem2_2(int[] a) {
		
		int l = 0, m = 0, r = a.length - 1;
		
		while(m <= r) {
			 if (a[m] == 0) {
				 int tmp = a[l];
			        a[l] = a[m];
			        a[m] = tmp;
	                l++;
	                m++;
	            } else if (a[m] == 1) {
	                m++;
	            } else {
	            	int tmp = a[m];
	                a[m] = a[r];
	                a[r] = tmp;
	                r--;
	            }
		}
		
		return a;
	}
	
	
	public String Problem3(int[] a, int goalSum) {
		
		Arrays.sort(a);
		
		int l = 0, r = a.length - 1;
		
		ArrayList<String> st = new ArrayList<String>();
		
		while(r > l) {
			if(a[r] + a[l] == goalSum) {
				st.add("(" + a[l] + "," + a[r]+")");
				l++;
				r--;
			}else if(a[r] + a[l] < goalSum) {
				l++;
			}else {
				r--;
			}
		}
		
		if(st.size() != 0) {
			return true + st.toString();
		}
		else {
			return false + "";
		}
	}
}



























